objs=symtable.o UnitTest.o
executor=UnitTest
LinkTable:$(objs)
	gcc -o $(executor) $(objs)
symtable.o:symtable.c symtable.h
	gcc -c symtable.c
UnitTest.o:UnitTest.c
	gcc -c UnitTest.c
clear:
	rm $(executor) $(objs)
