//linkTable.h

//链表的节点结构
struct Link
{
    char* Key;
    void* Value;
    struct Link* Next;
};

//作用是建立新的链表，如果内存不够就返回NULL pointer，否则返回结构体指针
struct Link* LinkTable_new(void);

//作用是释放链表由指针oLink指向的，链表所占用的内存。包括所以已经分配的结构体
void LinkTable_free(struct Link* oLink);

//返回链表中节点的个数
int LinkTable_getLength(struct Link* oLink);

//如果在链表中不存在pcKey，则将pcKey和pvValue插入链表并返回1
//如果没有内存空间或pvKey已经存在于链表之中则返回0
int Link_put(struct Link* oLink, 
	     const char* pcKey, 
	     const void* pvValue);

//在链表中查找pcKey，如果找到则将其对应的旧值替换为pvValue并返回指向旧值的指针。
//如果找不到则返回NULL pointer
void* Link_replace(struct Link* oLink, 
		   const char* pcKey, 
		   const void* pvValue);

//在链表中查找pcKey，如果找到则返回1，否则返回0.该操作不对链表做任何修改
int Link_constains(struct Link* oLink, 
		   const char* pcKey);

//在链表中查找pcKey，如果找到则返回其对应的值，否则返回NULL pointer。
//该操作不对链表做任何修改
void* Link_get(struct Link* oLink, 
	       const char* pcKey);

//在链表中查找pcKey，如果找到则删除pcKey和对应的值，并返回指向旧值的指针。
//否则返回NULL pointer
void* Link_remove(struct Link* oLink, 
		  const char* pcKey);

//测试使用，显示链表中所有的元素
void test_showAll(struct Link* oLink);
